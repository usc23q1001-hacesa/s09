from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('<int:id>/', views.grocery_item_details, name = 'grocery_item_details'),
    path('register/', views.register, name = 'register'),
    path('change_password/', views.change_password, name = 'change_password'),
    path('login/', views.login_view, name = 'login'),
    path('logout/', views.logout_view, name = 'logout'),

]
