from django.shortcuts import render, redirect
from .models import GroceryItem
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

# Create your views here.
def index(request):
    template = 'djangopractice/index.html'
    items = GroceryItem.objects.all()
    context = {'items': items}
    return render(request, template, context)

def grocery_item_details(request, id):
    template = 'djangopractice/grocery_item_details.html'

    try:
        item = GroceryItem.objects.get(id = id)
    except GroceryItem.DoesNotExist:
        item = None
    
    context = {'item': item, 'id': id}  
    return render(request, template, context)

def register(request):
    template = 'djangopractice/register.html'

    try:
        user = User.objects.get(username = 'kimminjeong')
    except User.DoesNotExist:
        user = None

    if user is not None:
        return render(request, template, {'is_registered': True})

    info = {
        'username': 'kimminjeong',
        'first_name': 'Minjeong',
        'last_name': 'Kim',
        'is_staff': False,
        'is_active': True
    }
    user = User(**info)
    user.set_password('P@ssw0rd')
    user.save()
    context = {
        "first_name": user.first_name,
        "last_name": user.last_name
    }
    return render(request, template, context)

def change_password(request):
    template = 'djangopractice/change_password.html'
    is_user_authenticated = False
    credentials = {
        'username':'kimminjeong',
        'password': 'P@ssw0rd',
    }
    new_password = 'P@ssw0rd789'

    user = authenticate(**credentials)
    if user is not None:
        user.set_password(new_password)
        user.save()
        is_user_authenticated = True

    context = {
        'is_authenticated': is_user_authenticated
    }
    return render(request, template, context)

def login_view(request):
    template = 'djangopractice/login.html'
    credentials = {
        'username': 'kimminjeong',
        'password': 'P@ssw0rd',
    }

    user = authenticate(**credentials)
    if user is not None:
        login(request, user)
        return redirect("index")
    
    return render(request, template, {})

def logout_view(request):
    logout(request)
    return redirect('index')