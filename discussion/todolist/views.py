from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import ToDoItem
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

# Create your views here.
def index(request):
    todoitem_list = ToDoItem.objects.all()
    context = { 
        'todoitem_list': todoitem_list,
        'user': request.user,
    }
    return render(request, 'todolist/index.html', context)

def todoitem(request, todoitem_id):
    todoitem = model_to_dict(ToDoItem.objects.get(pk = todoitem_id))
    return render(request, 'todolist/todoitem.html', todoitem)

def register(request):
    template = 'todolist/register.html'

    try:
        user = User.objects.get(username = 'kimminjeong')
    except User.DoesNotExist:
        user = None

    if user is not None:
        return render(request, template, {'is_registered': True})

    info = {
        'username': 'kimminjeong',
        'first_name': 'Minjeong',
        'last_name': 'Kim',
        'is_staff': False,
        'is_active': True
    }
    user = User(**info)
    user.set_password('P@ssw0rd')
    user.save()
    context = {
        "first_name": user.first_name,
        "last_name": user.last_name
    }
    return render(request, template, context)

def change_password(request):
    template = 'todolist/change_password.html'
    is_user_authenticated = False

    user = authenticate(username = 'kimminjeong', password = 'P@ssw0rd789')
    if user is not None:
        user.set_password('P@ssw0rd')
        user.save()
        is_user_authenticated = True

    context = {
        'is_authenticated': is_user_authenticated
    }
    return render(request, template, context)

def login_view(request):
    credentials = {
        'username': 'kimminjeong',
        'password': 'P@ssw0rd',
    }

    user = authenticate(**credentials)
    if user is not None:
        login(request, user)
        return redirect("index")
    
    return render(request, 'todolist/login.html', {})

def logout_view(request):
    logout(request)
    return redirect('index')